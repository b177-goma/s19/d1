// console.log('Hello Wrold');

// Exponent Operator
// An exponent operator is added to simplify the calculation for the exponent of a given number
const firstNum = Math.pow(8,2);
console.log(firstNum);

// ES6 update
const secondNum = 8 ** 2;
console.log(secondNum);

//Template Literals
// Allow developer to write strings without 

let name = 'John';
// Hello John! Welcome to programming!

// let message = "Hello" + name + "! Welcome to programming!"

// console.log("Message without template literalas: \n" + message)

// uses backticks (``)
// variables are palces inside a placeholder (${})
let message = `Hello ${name}! Welcom to programming`;

console.log(`Message with template: ${message}`);

//Multi-line using template literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account: ${principal * interestRate}`);

// [Section] Array Destructuring

const fullname =["Juan", "Tolits", "Dela Cruz"];

console.log(fullname[0]);
console.log(fullname[1]);
console.log(fullname[2]);

// Hello Juan Tolits Dela Cruz! It's good to see you again.
console.log("Hello " + fullname[0] + " " + fullname[1] + " " + fullname[2] + "! Its good to see you again.");

// Using Array Destructuring
const [firstname, middlename, lastname] = fullname;

console.log(firstname);
console.log(middlename);
console.log(lastname);

// using template literals and array destructing
console.log(`Hello ${firstname} ${middlename} ${lastname}! It's nice to see you again.`)

// [Section] Object Destructuring

const person ={
	givenName: "Jane",
	maidenName: "Miles",
	familyName: "Doe"
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Hello Jane Miles Doe! It's good to see you again. 
console.log(`Helo ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
// this should be exact prperty name of the object.
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you again.`);

function getfullName({givenName, maidenName, familyName}){
	console.log(`This is printed inside a function:`)
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getfullName(person);